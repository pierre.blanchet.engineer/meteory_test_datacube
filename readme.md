### Welcome to Meteory's quick data cube test

At meteory we work a lot with image analysis combined to a time serie analysis. This means that for a given area of interest we have many images across time. 

Your mission is to take the file source_data.nc and use the data inside to divide the area into 3 cluster. The file looks like this :

![1674145977943](image/readme/1674145977943.png)

the bands 'B02','B03' etc and DSWI,EVI, etc all represents parameters mesured by satellites on these fields. For example plotting the NDVI (a vegetation indicator) across 4 dates looks like this :

![1674146096602](image/readme/1674146096602.png)

Yout mission is therefore to use all this data (all variables **and** all dates) to create **1** image that will cluster the similar pixels. You can use any library you want. We want to see some great datascience so any other analysis on this data is welcomed ;)
